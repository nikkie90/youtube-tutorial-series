### Set kubeconfig file context for kubectl

    chmod 700 ~/Downloads/test-kubeconfig.yaml
    export KUBECONFIG=~/Downloads/test-kubeconfig.yaml

### Install Mongodb chart

    helm repo add bitnami https://charts.bitnami.com/bitnami    
    helm search repo bitnami
    helm install mongodb --values test-mongodb.yaml bitnami/mongodb

### Install Nginx Ingress Controller 

    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm repo update
    helm install nginx-ingress ingress-nginx/ingress-nginx

_Repo Link:_ https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx    

### Install Nginx Ingress Controller [DEPRECATED]   

    helm repo add stable https://kubernetes-charts.storage.googleapis.com/
    helm install nginx-ingress stable/nginx-ingress --set controller.publishService.enabled=true

_Repo Link:_ https://github.com/helm/charts/tree/master/stable/nginx-ingress    

